﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Democracy.Models
{
    public class Candidate
    {
        [Key]
        public int CandidateId { get; set; }

        public int VotingId { get; set; }

        public int UserId { get; set; }

        //TODO Corregir QuantityVotes antes de reiniciar la base de datos.
        public int QuatityVotes { get; set; }

        public virtual Voting Voting { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<VotingDetail> VotingDetails { get; set; }
    }
}