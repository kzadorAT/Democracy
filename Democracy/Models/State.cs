﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Democracy.Models
{
    public class State
    {
        /// <summary>
        /// Gets or sets the state id
        /// </summary>
        [Key]
        public int StateId { get; set; }

        /// <summary>
        /// Gets or sets the state description
        /// </summary>
        [Required(ErrorMessage ="The field {0} is required")]
        [StringLength(50, ErrorMessage = "The field {0} can contain maximum {1} and minimun {2} characters", MinimumLength =3)]
        [Display(Name = "State description")]
        public string Description { get; set; }

        public virtual ICollection<Voting> Votings { get; set; }
    }
}